class Machine {
    // Constructeur
    constructor() {
        this.listeTaches = [];
        this.dureeTotal = 0;
    }

    // Méthode qui permet d'assigner une tâche à la machine en recalculant la durée total
    assignerTache(tache) {
        this.listeTaches.push(tache);
        this.dureeTotal += tache.duree;
    }

    // Méthode qui permet de remettre la machine à zéro 
    reset() {
        this.listeTaches = [];
        this.dureeTotal = 0;
    }
}