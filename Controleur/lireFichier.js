// Fonction qui permet de lire le fichier (pour le type 1)
function lireFichier() {
    // Stockage du fichier de l'utilisateur dans la variable file
    var file = document.getElementById("fileInput").files[0];
    // Si le fichier est correct
    if (file) {
        // Instanciation d'un objet FileReader pour lire le fichier
        var reader = new FileReader();
        // Lecture du fichier au format UTF-8
        reader.readAsText(file, "UTF-8");
        // A la fin de la lecture du fichier, faire
        reader.onload = function (evt) {
            if(validerType1(evt.target.result)) {
                var i = lireContenu(evt.target.result);
                resultat(i);
            } else {
                alert("Le fichier n'est pas au bon format.");
            }
        }
        // Si on rencontre un erreur avec le fichier, faire
        reader.onerror = function (evt) {
            alert("Fichier erroné.")
        }
    }
}
