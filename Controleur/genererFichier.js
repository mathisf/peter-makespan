// Fonction qui génère un fichier et l'envoi à l'utilisateur,
// avec pour paramètre le nom du fichier et son contenu
function genererFichier(nomfichier, contenu) {
    // Création d'un lien
    var element = document.createElement('a');

    // Assigne le contenu et le nom du fichier au lien
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(contenu));
    element.setAttribute('download', nomfichier);

    // Le lien devient invisible
    element.style.display = 'none';

    // Ajout du lien sur la page
    document.body.appendChild(element);

    // Simule un clique sur le lien du fichier
    element.click();

    // Supression du lien
    document.body.removeChild(element);
}
