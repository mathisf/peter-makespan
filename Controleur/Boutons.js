// Bouton "Afficher le résultat" pour le type 1
if(document.getElementById("fileInput")!=null){
    // Pour le clique sur le bouton résultat
    document.getElementById("resultatBouton").addEventListener("click", function(){
        // Lecture du fichier
        lireFichier();
    });
    // Quand on charge un fichier différent
    document.getElementById("fileInput").addEventListener("change", function() {
        // Supression des anciens résultats (sur l'interface)
        resetResultat();
        resetDetail();
    })
}

// Bouton "Afficher le résultat" pour le type 2
if(document.getElementById("textInput")!=null){
    // Pour le clique sur le bouton résultat
    document.getElementById("resultatBouton").addEventListener("click", function(){
        if(validerType2(document.getElementById("textInput").value)) {
            // Lecture du champs de texte
            var i = lireContenu(document.getElementById("textInput").value);
            // Affichage du résultat de l'instance
            resultat(i);
        } else {
            document.getElementById("textInput").style.borderColor = "red";
            document.getElementById("textInput").focus();
            alert("Mauvais format.")
        }
    });
    document.getElementById("textInput").addEventListener("keyup", function(){
        if(validerType2(document.getElementById("textInput").value)) {
            document.getElementById("textInput").style.borderColor = "green";
        } else {
            document.getElementById("textInput").style.borderColor = "red";
        }
    })
}

// Bouton "Afficher les détails" pour le type 1 et 2
if(document.getElementById("detail")!=null){
    // Les détails sont masqués
    document.getElementById("detail").style.display = "none";
    // Pour le bouton de détails
    document.getElementById("detailBouton").addEventListener("click", function(){
        // Affiche ou masque les détails
        afficherOuMasquerDetails();
    })
}

if(document.getElementById("genererFichier")!=null){
    document.getElementById("genererFichier").addEventListener("click", function(){
        // Récupère les données de chaques champs du formulaire (en les transformant en integer si besoin)
        var m = document.getElementById("m").value;
        var n = document.getElementById("n").value;
        var k = document.getElementById("k").value;
        var min = document.getElementById("min").value;
        var max = document.getElementById("max").value;
        var nom_fichier = document.getElementById("nomFichier").value;

        if (m != "" && n != "" && k != "" && min != "" && max != "" && nom_fichier != "") {
            m = parseInt(m);
            n = parseInt(n);
            k = parseInt(k);
            min = parseInt(min);
            max = parseInt(max);

            if (!isNaN(m) && !isNaN(n) && !isNaN(k) && !isNaN(min) && !isNaN(max)) {
                var instances = [];
                var taches = [];
    
                // Pour chaque instances, assigne des tâches aléatoire d'une durée entre min et max inclu
                for (let i = 0; i < k; i++) {
                    taches = [];
                    for (let j = 0; j < n; j++) {
                        var t = new Tache(aleatoireInterval(min,max));
                        taches.push(t);
                    }
                    var instance = new Instance(m, taches);
                    instances.push(instance);
                }
    
                // Créer un fichier avec les résultats des instances et l'envoi a l'utilisateur
                genererResultat(instances, nom_fichier + ".txt");
            } else {
                alert("Les valeurs des champs sont invalides.");
            }
        } else {
            alert("Veuillez remplir tout les champs.");
        }
    });
}