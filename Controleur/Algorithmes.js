function LSA(instance) {
    // m prend pour valeur la première machine disponible de l'instance
    var m = instance.premiereMachineDisponible();
    // Pour chaque taches de l'instance
    for (let i = 0; i < instance.taches.length; i++) {
        // Assigner une tache à la première machine disponible
        m.assignerTache(instance.taches[i]);
        // m prend pour valeur la première machine disponible de l'instance
        m = instance.premiereMachineDisponible();
    }
    // Retourne la durée maximum entre les machines de l'instances
    return Math.max.apply(Math, instance.machines.map(function(o){return o.dureeTotal;}));
}

function LPT(instance) {
    // Tri les tâches dans l'ordre décroissant des durées
    var t = instance.taches.sort(instance.comparerDecroissant);
    // Même algorithme que LSA
    var m = instance.premiereMachineDisponible();
    for (let i = 0; i < t.length; i++) {
        m.assignerTache(t[i]);
        m = instance.premiereMachineDisponible();
    }
    return Math.max.apply(Math, instance.machines.map(function(o){return o.dureeTotal;}));
}

function MyAlgo(instance) {
    // Tri les tâches dans l'ordre croissant des durées
    var t = instance.taches.sort(instance.comparerCroissant);
    // Même algorithme que LSA
    var m = instance.premiereMachineDisponible();
    for (let i = 0; i < t.length; i++) {
        m.assignerTache(t[i]);
        m = instance.premiereMachineDisponible();
    }
    return Math.max.apply(Math, instance.machines.map(function(o){return o.dureeTotal;}));
}