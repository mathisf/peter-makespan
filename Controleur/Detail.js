function afficherOuMasquerDetails() {
  if(document.getElementById("detail").style.display == "block") {
      document.getElementById("detail").style.display = "none";
      document.getElementById("detailBouton").innerHTML = "Afficher les détails";
  } else {
      document.getElementById("detail").style.display = "block";
      document.getElementById("detailBouton").innerHTML = "Masquer les détails";
  }
}
